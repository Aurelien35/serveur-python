FROM python
COPY hello.py /hello.py
COPY requirements.txt /requirements.txt
RUN pip install -r requirements.txt
CMD python3 /hello.py