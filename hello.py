from flask import Flask, request  # pip install flask
app = Flask(__name__)
import redis
import time


cache = redis.Redis(host='redis', port=6379)

@app.route("/", methods=['POST'])
def record():
	request.get_data()
	data = request.data
	timestamp = time.time()
	cache.set(timestamp, data)
	return data
#commentaire

@app.route("/", methods=['GET'])
def getdata():
	return (str(cache.keys()))

if __name__ == "__main__":
	app.run(host='0.0.0.0')


